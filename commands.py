import tcod
from entity import get_blocking_entities_at_location
from game_messages import Message

def move(entity, dx, dy, game_map, entities):
    def inner_move():
        results = []
        destination_x = entity.x + dx
        destination_y = entity.y + dy

        if not game_map.is_blocked(destination_x, destination_y):
            target = get_blocking_entities_at_location(entities, destination_x,
                                                       destination_y)
            if target:
                attack_results = entity.fighter.attack(target, dx, dy)
                results.extend(attack_results)
            else:
                entity.move(dx, dy)
                if entity.fighter.charging:
                    results.extend(entity.fighter.charge(dx, dy))
        if entity.name == "Player":
            results.append({'fov_recompute': True})

        return results
    return inner_move

def pickup(actor, entities, message_log):
    def inner_pickup():
        results = []
        for entity in entities:
            if entity.item and entity.x == actor.x and entity.y == actor.y:
                pickup_results = actor.inventory.add_item(entity)
                entities.remove(entity)
                results.extend(pickup_results)
                break
        else:
            message_log.add_message(Message('There is nothing here to pick up.',
                                            tcod.yellow))
        return results
    return inner_pickup

def drop(actor, item, entities):
    def inner_drop():
        entities.append(item)
        return actor.inventory.drop_item(item)
    return inner_drop

def use(player, item_entity, **kwargs):
    if item_entity.item.targeting and not (kwargs.get('target_x') or kwargs.get('target_y')):
        return [{'targeting': item_entity}]
    def inner_use():
        use_results = player.inventory.use(item_entity, **kwargs)
        equip = use_results[0].get('equip')
        if equip:
            equip_results = player.equipment.toggle_equip(equip)
            for equip_result in equip_results:
                equipped = equip_result.get('equipped')
                dequipped = equip_result.get('dequipped')

                if equipped:
                    use_results.append({'message': Message(
                        'You equipped the {0}'.format(equipped.name))})

                if dequipped:
                    use_results.append({'message': Message(
                        'You dequipped the {0}'.format(dequipped.name))})
        return use_results
    return inner_use
