from random import randint


def random_choice_index(chances):
    random_chance = randint(1, sum(chances))

    running_sum = 0
    choice = 0
    for w in chances:
        running_sum += w

        if random_chance <= running_sum:
            return choice
        choice += 1

def from_dungeon_level(table, dungeon_level):
    """Take table of num. of occurences and levels, and return the number of occurences,
    if the current level is same or bigger then level in table

    The table is a list of lists, in format [[occurences, level],...].
    We read the table from back and once dungeon_level matches level in table,
    we retun the number of occurences.
    """
    for (value, level) in reversed(table):
        if dungeon_level >= level:
            return value
    return 0

def random_choice_from_dict(choice_dict):
    choices = list(choice_dict.keys())
    chances = list(choice_dict.values())

    return choices[random_choice_index(chances)]
