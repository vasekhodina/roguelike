# roguelike

My first roguelike, made with rogueliketutorials.com

Controls
--------

* vi keys (hjkl) - to move 
* i - to see inventory
* d - to drop things
* enter - to use stairs
* g - to pickup
* r - to start charging at the enemy
* left Alt + Enter for fullscreen
* z - wait a turn
