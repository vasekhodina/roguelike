import tcod as libtcod

from game_messages import Message
from render_functions import RenderOrder

class Fighter:
    def __init__(self, hp, defense, power, stamina, attack_fatigue=5, stamina_recovery=1):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense
        self.base_power = power
        self.base_max_stamina = stamina
        self.stamina = stamina
        self.attack_fatigue = attack_fatigue
        self.base_stamina_recovery = stamina_recovery
        self.charging = False
        self.max_charge_bonus = 4
        self.charge_bonus = 0
        self.charge_vector = None

    @property
    def max_hp(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.max_hp_bonus
        else:
            bonus = 0

        return self.base_max_hp + bonus

    @property
    def power(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.power_bonus
        else:
            bonus = 0

        return self.base_power + bonus + self.charge_bonus

    @property
    def defense(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.defense_bonus
        else:
            bonus = 0

        return self.base_defense + bonus

    @property
    def max_stamina(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.max_stamina_bonus
        else:
            bonus = 0
        return self.base_max_stamina + bonus

    def take_damage(self, amount):
        results = []

        self.hp -= amount
        if self.hp <= 0:
            message = self.die()
            results.append({'message': message})

        return results

    def attack(self, target, dx=None, dy=None):
        results = []
        if self.stamina >= self.attack_fatigue:
            if (self.charge_bonus > 0 and self.charge_vector and
                self.charge_step_valid(self.charge_vector, (dx, dy))): 
                self.tire(1)
                results.append(
                        {'message': Message('{0} hits {1} with a mighty charge.'.format(
                            self.owner.name.capitalize(), target.name), libtcod.yellow)
                        })
            elif self.charging:
                results.extend(self.toggle_charge())
            self.tire(self.attack_fatigue)
            damage = self.power - target.fighter.defense
            if damage > 0:
                results.append(
                        {'message': Message('{0} attacks {1} for {2} hit points.'.format(
                            self.owner.name.capitalize(), target.name, str(damage)),
                                    libtcod.white)
                        })
                results.extend(target.fighter.take_damage(damage))
            else:
                results.append(
                        {'message': Message('{0} attacks {1} but does no damage.'.format(
                            self.owner.name.capitalize(), target.name), libtcod.yellow)
                        })
        else:
            results.append(
                    {'message': Message('{0} attacks faintly due to exhaustion.'.format(
                        self.owner.name.capitalize()), libtcod.orange)
                    })

        self.charge_bonus = 0
        self.charge_vector = None
        self.charging = False
        return results

    def heal(self, amount):
        self.hp += amount

        if self.hp > self.max_hp:
            self.hp = self.max_hp

    def tire(self, amount):
        self.stamina -= amount

    def recover_stamina(self, bonus_amount=0):
        self.stamina += self.base_stamina_recovery + bonus_amount

        if self.stamina > self.max_stamina:
            self.stamina = self.max_stamina

    def toggle_charge(self):
        results = []
        if self.charging:
            self.charging = False
            self.charge_vector = None
            self.charge_bonus = 0
            results.append(
                    {'message': Message('{0} stops charging.'.format(
                        self.owner.name.capitalize()), libtcod.yellow)
                    })
        else:
            self.charging = True
            results.append(
                    {'message': Message('{0} starts charging.'.format(
                        self.owner.name.capitalize()), libtcod.yellow)
                    })
        return results

    def charge(self, dx, dy):
        results = []
        if self.charge_vector:
            if not self.charge_step_valid(self.charge_vector, (dx, dy)):
                results.extend(self.toggle_charge())
                return results
        self.charge_vector = (dx, dy)
        if self.stamina <= 0:
            results.append(
                    {'message': Message('{0} is to tired to charge.'.format(
                        self.owner.name.capitalize()), libtcod.yellow)
                    })
            results.extend(self.toggle_charge())
        if self.charge_bonus < self.max_charge_bonus:
            self.charge_bonus += 1
        self.tire(2)
        return results

    def charge_step_valid(self, old_vec, vec):
        """Check if we're maintaining momentum during charge
        Ie. if we've charged north last turn, we can continue only N, NW, NE.
        """
        if 0 in old_vec:
            for i in 0, 1:
                if old_vec[i] != 0 and old_vec[i] != vec[i]:
                    return False
        else:
            final_vec = list(map(lambda a, b: a + b, old_vec, vec))
            if 0 in final_vec:
                return False
        return True

    def die(self):
        death_message = f'{self.owner.name.capitalize()} is dead!'
        self.owner.char = '%'
        self.owner.color = libtcod.dark_red
        self.owner.ai = None
        self.owner.name = 'remains of ' + self.owner.name
        self.owner.blocks = False
        self.owner.render_order = RenderOrder.CORPSE
        return Message(death_message, libtcod.orange)
