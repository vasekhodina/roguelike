from game_states import GameStates

class Player:
    def __init__(self):
        self.scheduled_action = None

    def take_turn(self):
        if self.scheduled_action:
            return self.scheduled_action()
        else:
            return []

    def schedule_action(self, action_function):
        self.scheduled_action = action_function
