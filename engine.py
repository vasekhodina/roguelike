import tcod as libtcod
from input_handlers import handle_keys, handle_mouse, handle_main_menu
from loader_functions.initialize_new_game import get_constants, get_game_variables
from loader_functions.data_loaders import load_game, save_game
from entity import Entity, get_blocking_entities_at_location 
from render_functions import render_all, clear_all
from fov_functions import initialize_fov, recompute_fov
from game_states import GameStates
from death_functions import kill_monster, kill_player
from game_messages import Message, MessageLog
from menus import main_menu, message_box
import commands


def main():
    constants = get_constants()

    libtcod.console_set_custom_font('arial10x10.png', libtcod.FONT_TYPE_GREYSCALE | libtcod.FONT_LAYOUT_TCOD) 
    libtcod.console_init_root(constants['screen_width'], constants['screen_height'],
            constants['window_title'], False, libtcod.RENDERER_SDL2, vsync=False)

    con = libtcod.console.Console(constants['screen_width'], constants['screen_height'])
    panel = libtcod.console.Console(constants['screen_width'], constants['panel_height'])

    player = None
    entities = []
    game_map = None
    message_log = None
    game_state = None

    show_main_menu = True
    show_load_error_message = False

    main_menu_background_image = libtcod.image_load('menu_background.png')

    key = libtcod.Key()
    mouse = libtcod.Mouse()

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if show_main_menu:
            main_menu(con, main_menu_background_image, constants['screen_width'],
                      constants['screen_height'])

            if show_load_error_message:
                message_box(con, 'No save game to load', 50, constants['screen_width'],
                            constants['screen_height'])

            libtcod.console_flush()

            action = handle_main_menu(key)

            new_game = action.get('new_game')
            load_saved_game = action.get('load_game')
            exit_game = action.get('exit')

            if show_load_error_message and (new_game or load_saved_game or exit_game):
                show_load_error_message = False
            elif new_game:
                player, entities, game_map, message_log, game_state = get_game_variables(constants)
                game_state = GameStates.PLAYERS_TURN

                show_main_menu = False
            elif load_saved_game:
                try:
                    player, entities, game_map, message_log, game_state = load_game()
                    show_main_menu = False
                except FileNotFoundError:
                    show_load_error_message = True
            elif exit_game:
                break

        else:
            con.clear(fg=(63, 127, 63))
            play_game(player, entities, game_map, message_log, game_state, con, panel,
                      constants)

            show_main_menu = True

def play_game(player, entities, game_map, message_log, game_state, con, panel, constants):
    fov_recompute = True
    new_turn = True

    fov_map = initialize_fov(game_map)

    key = libtcod.Key()
    mouse = libtcod.Mouse()

    game_state = GameStates.PLAYERS_TURN
    previous_game_state = game_state

    targeting_item = None

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if fov_recompute:
            recompute_fov(fov_map, player.x, player.y, constants['fov_radius'],
                          constants['fov_light_walls'], constants['fov_algorithm'])

        render_all(con, panel, entities, player, game_map, fov_map, fov_recompute,
                   message_log, constants['screen_width'], constants['screen_height'],
                   constants['bar_width'], constants['panel_height'], constants['panel_y'],
                   mouse, constants['colors'], game_state)

        fov_recompute = False

        libtcod.console_flush()

        clear_all(con, entities)

        if new_turn:
            for entity in entities:
                if entity.fighter:
                    entity.fighter.recover_stamina()
            new_turn = False

        # --- PLAYER INPUT ---
        action = handle_keys(key, game_state)
        mouse_action = handle_mouse(mouse)

        move = action.get('move')
        pickup = action.get('pickup')
        show_inventory = action.get('show_inventory')
        drop_inventory = action.get('drop_inventory')
        inventory_index = action.get('inventory_index')
        take_stairs = action.get('take_stairs')
        exit = action.get('exit')
        fullscreen = action.get('fullscreen')
        wait = action.get('wait')
        charge = action.get('charge')

        left_click = mouse_action.get('left_click')
        right_click = mouse_action.get('right_click')

        player_turn_results = []

        if move and game_state == GameStates.PLAYERS_TURN:
            dx, dy = move
            player.schedule_action(commands.move(player, dx, dy, game_map, entities))
            game_state = GameStates.ENEMY_TURN
            new_turn = True
        elif wait:
            player.schedule_action(None)
            new_turn = True
            game_state = GameStates.ENEMY_TURN
        elif pickup and game_state == GameStates.PLAYERS_TURN:
            player.schedule_action(commands.pickup(player, entities, message_log))
            new_turn = True
            game_state = GameStates.ENEMY_TURN

        if charge:
            player_turn_results.extend(player.fighter.toggle_charge())

        if show_inventory:
            previous_game_state = game_state
            game_state = GameStates.SHOW_INVENTORY

        if drop_inventory:
            previous_game_state = game_state
            game_state = GameStates.DROP_INVENTORY

        if (inventory_index is not None and previous_game_state != GameStates.PLAYER_DEAD and
                inventory_index < len(player.inventory.items)):
            item = player.inventory.items[inventory_index]

            if game_state == GameStates.SHOW_INVENTORY:
                player.schedule_action(commands.use(player, item,
                                                           entities=entities,
                                                           fov_map=fov_map))
                if type(player.scheduled_action) == list:
                    targeting = player.scheduled_action[0].get('targeting')
                    previous_game_state = GameStates.PLAYERS_TURN
                    game_state = GameStates.TARGETING
                    targeting_item = targeting

                    message_log.add_message(targeting_item.item.targeting_message)
                else:
                    game_state = GameStates.ENEMY_TURN
            elif game_state == GameStates.DROP_INVENTORY:
                player.schedule_action(commands.drop(player, item, entities))
                game_state = GameStates.ENEMY_TURN

        if take_stairs and game_state == GameStates.PLAYERS_TURN:
            for entity in entities:
                if entity.stairs and entity.x == player.x and entity.y == player.y:
                    entities = game_map.next_floor(player, message_log, constants)
                    fov_map = initialize_fov(game_map)
                    fov_recompute = True
                    con.clear(fg=(63, 127, 63))

                    break
            else:
                message_log.add_message(Message('There are no stairs here.', libtcod.yellow))

        if game_state == GameStates.TARGETING:
            if left_click:
                target_x, target_y = left_click
                player.schedule_action(commands.use(player, targeting_item,
                                                           entities=entities, fov_map=fov_map,
                                                           target_x=target_x,
                                                           target_y=target_y))
                game_state = GameStates.ENEMY_TURN
            elif right_click:
                player_turn_results.append({'targeting_cancelled': True})

        if exit:
            if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
                game_state = previous_game_state
            elif game_state == GameStates.TARGETING:
                player_turn_results.append({'targeting_cancelled': True})
            else:
                save_game(player, entities, game_map, message_log, game_state)

                return True

        if fullscreen:
            libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())
        # --- END OF PLAYER INPUT ---

        # --- PLAYER TURN RESULTS ---
        for player_turn_result in player_turn_results:
            message = player_turn_result.get('message')
            targeting = player_turn_result.get('targeting')
            targeting_cancelled = player_turn_result.get('targeting_cancelled')

            if message:
                message_log.add_message(message)

            if targeting_cancelled:
                game_state = previous_game_state

                message_log.add_message(Message('Targeting cancelled'))
            new_turn = True
        # --- END OF PLAYER TURN RESULTS ---

        if game_state == GameStates.ENEMY_TURN:
            for entity in entities:
                if entity.ai:
                    entity_turn_results = entity.ai.take_turn(player, fov_map, game_map,
                                                             entities)
                else:
                    entity_turn_results = []
                entity_turn_results = entity.process_turn()

                for entity_turn_result in entity_turn_results:
                    message = entity_turn_result.get('message')
                    if message:
                        message_log.add_message(message)
                        if player.fighter.hp <= 0:
                            game_state = GameStates.PLAYER_DEAD
                            break
                    if entity_turn_result.get('fov_recompute'):
                        fov_recompute = entity_turn_result.get('fov_recompute')

                if player.fighter.hp <= 0:
                    game_state = GameStates.PLAYER_DEAD
                    break

            else:
                game_state = GameStates.PLAYERS_TURN

if __name__ == '__main__':
    main()
